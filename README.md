# CSS-website

The new website for the Luther Computer Science Society

To Do:
 - make website more pleasing to look at
 - stylize navigation bar
 - add bootstrap
 - create server
 - transfer to Jinja 2
 - create other pages
 - add POST and DELETE for proposals
 - create auto-moderator algorithm
